variable region {}                               
variable instance_type {}
variable user {}

 #Задаем регион для работы

provider "aws" {
  region     = var.region
}

#задаем  последнию версию Амазон Линух образа
data "aws_ami" "latest_ubuntu_image" {     
  most_recent = true
  owners      = ["099720109477"]
  filter {
    name = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server*"]
  }
   filter {
        name   = "virtualization-type"
        values = ["hvm"]
    }

  }


 # Чекаем что образ корректно отображается   
  output "aws_ami_id" {                                  
  value = data.aws_ami.latest_ubuntu_image.id
  }

# делаем инстансы
resource "aws_instance" "node" {                 
count = 3
  ami                         = data.aws_ami.latest_ubuntu_image.id 
  instance_type               = var.instance_type
  key_name                    = "gitlab-runner"
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.tf51-sg.id]

 tags = {
    Name = "node${count.index +1}"
  }

  root_block_device {
    volume_size = 8
    volume_type = "gp2"
  }


#Записываем нужные данные в локал файл
 provisioner "local-exec" {     
    command = "echo Private IP: ${self.private_ip}  Public IP  ${self.public_ip}    Instance name : ${self.tags.Name} >> ./host.list" 
  }


# Задаем SSH данные для ремоут доступа.  Я правда ХЗ  зачем мы это делаем, красивее через Startup script запустить (user data)  
  #AWS   не рекомендует юзать Remote-exec
  connection {      
    type = "ssh"
    host = self.public_ip
    user = var.user
    private_key = file("/Users/fstep/gitlab-runner.pem")
  }

provisioner "remote-exec" {
    inline = [
      "sudo apt update",
      "sudo apt install nginx -y",
      "sudo chown -R $USER:$USER /var/www",
      "sudo echo 'Juneway Instance name : ${self.tags.Name}  Public IP  ${self.public_ip}' > /var/www/html/index.html ",
      "sudo systemctl enable nginx",
      "sudo systemctl start nginx",
    ]
}
#Для SSH  и Nginx нам нужна Security group.
}

# Запросим ID VPC
data "aws_vpc" "default" {
  default = true
}

 # Чекаем что  VPC ID корректно считался 
  output "aws_vpc_id" {                                  
  value = data.aws_vpc.default.id
  }


# Создадим Security group


resource "aws_security_group" "tf51-sg" { 
  name   = "tf51-sg"
  vpc_id = data.aws_vpc.default.id


  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 433
    to_port     = 433
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 10000
    to_port     = 20000
    protocol    = "udp"
    cidr_blocks = ["10.0.0.23/32"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

} 


output "instance_names" {
  value = aws_instance.node.*.tags.Name
}

  




  



